<?php

/* ::base.html.twig */
class __TwigTemplate_50d5bbcc8bb0fc03e79d19ed2c1336fb7b3b0dc50e96c0fe511be0c4ad083cd7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!doctype html>
<html lang=\"en\">
<head>
    <!-- Required meta tags -->
    <meta charset=\"utf-8\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">

    <!-- Material Design for Bootstrap fonts and icons -->
    <link rel=\"stylesheet\" href=\"https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons\">

    <!-- Material Design for Bootstrap CSS -->
    <link rel=\"stylesheet\" href=\"https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css\" integrity=\"sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX\" crossorigin=\"anonymous\">
    <title>";
        // line 14
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 16
        echo "
</head>
<body>
<ul class=\"nav nav-tabs bg-primary justify-content-center\">
    <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"/\">Home</a>
    </li>
    <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"/db\">demo bundle</a>
    </li>
    <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"/details\">data Bundle</a>
    </li>
    ";
        // line 29
        if ($this->env->getExtension('security')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 30
            echo "    <li class=\"nav-item\">
        <a class=\"nav-link justify-content-end\" href=\"/logout\">logout</a>
    </li>
    ";
        } else {
            // line 34
            echo "    <li class=\"nav-item\">
        <a class=\"nav-link\" href=\"/login_form\">login</a>
    </li>
    ";
        }
        // line 38
        echo "</ul>
<div class=\"container\">
    ";
        // line 40
        $this->displayBlock('body', $context, $blocks);
        // line 41
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 42
        echo "</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>
<script src=\"https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js\" integrity=\"sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U\" crossorigin=\"anonymous\"></script>
<script src=\"https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js\" integrity=\"sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9\" crossorigin=\"anonymous\"></script>
<script>\$(document).ready(function() { \$('body').bootstrapMaterialDesign(); });</script>
</body>
</html>";
    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        echo "Welcome!";
    }

    // line 15
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 40
    public function block_body($context, array $blocks = array())
    {
    }

    // line 41
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 41,  106 => 40,  101 => 15,  95 => 14,  82 => 42,  79 => 41,  77 => 40,  73 => 38,  67 => 34,  61 => 30,  59 => 29,  44 => 16,  42 => 15,  38 => 14,  23 => 1,);
    }
}
/* */
/* <!doctype html>*/
/* <html lang="en">*/
/* <head>*/
/*     <!-- Required meta tags -->*/
/*     <meta charset="utf-8">*/
/*     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">*/
/* */
/*     <!-- Material Design for Bootstrap fonts and icons -->*/
/*     <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">*/
/* */
/*     <!-- Material Design for Bootstrap CSS -->*/
/*     <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">*/
/*     <title>{% block title %}Welcome!{% endblock %}</title>*/
/*     {% block stylesheets %}{% endblock %}*/
/* */
/* </head>*/
/* <body>*/
/* <ul class="nav nav-tabs bg-primary justify-content-center">*/
/*     <li class="nav-item">*/
/*         <a class="nav-link" href="/">Home</a>*/
/*     </li>*/
/*     <li class="nav-item">*/
/*         <a class="nav-link" href="/db">demo bundle</a>*/
/*     </li>*/
/*     <li class="nav-item">*/
/*         <a class="nav-link" href="/details">data Bundle</a>*/
/*     </li>*/
/*     {% if is_granted('IS_AUTHENTICATED_REMEMBERED') %}*/
/*     <li class="nav-item">*/
/*         <a class="nav-link justify-content-end" href="/logout">logout</a>*/
/*     </li>*/
/*     {% else %}*/
/*     <li class="nav-item">*/
/*         <a class="nav-link" href="/login_form">login</a>*/
/*     </li>*/
/*     {% endif %}*/
/* </ul>*/
/* <div class="container">*/
/*     {% block body %}{% endblock %}*/
/*     {% block javascripts %}{% endblock %}*/
/* </div>*/
/* */
/* <!-- Optional JavaScript -->*/
/* <!-- jQuery first, then Popper.js, then Bootstrap JS -->*/
/* <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>*/
/* <script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>*/
/* <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>*/
/* <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>*/
/* </body>*/
/* </html>*/
