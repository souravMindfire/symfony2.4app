<?php

/* UsersBundle:Security:login.html.twig */
class __TwigTemplate_3ab22a89565b72a30b8a80dde97cdc298fad58869caca90012fe925b6b4aca9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "UsersBundle:Security:login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        // line 3
        echo "
    <form action=\"";
        // line 4
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
        <div class=\"form-group\">
            <label for=\"exampleInputEmail1\" class=\"bmd-label-floating\">label-floating</label>
            <input type=\"text\" class=\"form-control\" aria-describedby=\"emailHelp\" name=\"_username\">
        </div>
        <div class=\"form-group\">
            <label for=\"exampleInputPassword1\" class=\"bmd-label-floating\">Password</label>
            <input type=\"password\" class=\"form-control\"  name=\"_password\">
        </div>
        <button type=\"submit\" class=\"btn btn-raised btn-primary\">login</button>
    </form>

";
    }

    public function getTemplateName()
    {
        return "UsersBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 4,  31 => 3,  28 => 2,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* {% block body %}*/
/* */
/*     <form action="{{ path('login_check') }}" method="post">*/
/*         <div class="form-group">*/
/*             <label for="exampleInputEmail1" class="bmd-label-floating">label-floating</label>*/
/*             <input type="text" class="form-control" aria-describedby="emailHelp" name="_username">*/
/*         </div>*/
/*         <div class="form-group">*/
/*             <label for="exampleInputPassword1" class="bmd-label-floating">Password</label>*/
/*             <input type="password" class="form-control"  name="_password">*/
/*         </div>*/
/*         <button type="submit" class="btn btn-raised btn-primary">login</button>*/
/*     </form>*/
/* */
/* {% endblock %}*/
