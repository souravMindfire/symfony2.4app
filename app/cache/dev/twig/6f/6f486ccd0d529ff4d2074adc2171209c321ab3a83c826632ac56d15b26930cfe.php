<?php

/* DataflowBundle:Details:index.html.twig */
class __TwigTemplate_42d9a7856f200f891db3881cbb481aeaa26e8c1736042d98b5e973e8b54b1811 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "DataflowBundle:Details:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Details list</h1>

    <table class=\"records_list\">
        <thead class=\"table table-borderless\">
            <tr>
                <th scope=\"col\">Id</th>
                <th scope=\"col\">Name</th>
                <th scope=\"col\">Time</th>
                <th scope=\"col\">Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entities"]) ? $context["entities"] : $this->getContext($context, "entities")));
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 17
            echo "            <tr>
                <td scope=\"row\"><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("details_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["entity"], "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entity"], "time", array()), "g:ia l M j, Y"), "html", null, true);
            echo "</td>
                <td>
                    <a class=\"btn btn-primary active\" role=\"button\" aria-pressed=\"true\" href=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("details_show", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">show</a>
                    <a class=\"btn btn-primary active\" role=\"button\" aria-pressed=\"true\" href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("details_edit", array("id" => $this->getAttribute($context["entity"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "        </tbody>
    </table>

        <ul>
        <li>
            <a class=\"btn btn-primary active\" role=\"button\" aria-pressed=\"true\" href=\"";
        // line 32
        echo $this->env->getExtension('routing')->getPath("details_new");
        echo "\">
                Create a new entry
            </a>
        </li>
    </ul>
    ";
    }

    public function getTemplateName()
    {
        return "DataflowBundle:Details:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 32,  81 => 27,  71 => 23,  67 => 22,  62 => 20,  58 => 19,  52 => 18,  49 => 17,  45 => 16,  31 => 4,  28 => 3,  11 => 1,);
    }
}
/* {% extends '::base.html.twig' %}*/
/* */
/* {% block body -%}*/
/*     <h1>Details list</h1>*/
/* */
/*     <table class="records_list">*/
/*         <thead class="table table-borderless">*/
/*             <tr>*/
/*                 <th scope="col">Id</th>*/
/*                 <th scope="col">Name</th>*/
/*                 <th scope="col">Time</th>*/
/*                 <th scope="col">Actions</th>*/
/*             </tr>*/
/*         </thead>*/
/*         <tbody>*/
/*         {% for entity in entities %}*/
/*             <tr>*/
/*                 <td scope="row"><a href="{{ path('details_show', { 'id': entity.id }) }}">{{ entity.id }}</a></td>*/
/*                 <td>{{ entity.name }}</td>*/
/*                 <td>{{ entity.time | date('g:ia l M j, Y')}}</td>*/
/*                 <td>*/
/*                     <a class="btn btn-primary active" role="button" aria-pressed="true" href="{{ path('details_show', { 'id': entity.id }) }}">show</a>*/
/*                     <a class="btn btn-primary active" role="button" aria-pressed="true" href="{{ path('details_edit', { 'id': entity.id }) }}">edit</a>*/
/*                 </td>*/
/*             </tr>*/
/*         {% endfor %}*/
/*         </tbody>*/
/*     </table>*/
/* */
/*         <ul>*/
/*         <li>*/
/*             <a class="btn btn-primary active" role="button" aria-pressed="true" href="{{ path('details_new') }}">*/
/*                 Create a new entry*/
/*             </a>*/
/*         </li>*/
/*     </ul>*/
/*     {% endblock %}*/
/* */
