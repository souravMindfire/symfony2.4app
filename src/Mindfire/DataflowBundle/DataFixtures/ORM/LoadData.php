<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 6/6/18
 * Time: 3:15 PM
 */
namespace Mindfire\DataflowBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mindfire\DataflowBundle\Entity\Details;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadData implements FixtureInterface,OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user=$manager->getRepository('UsersBundle:Login')->findByEmail('sourav');
        for ($i = 0; $i < 5; $i++) {
            $details=new Details();
            $details->setName('user'.$i);
            $details->setTime(new \DateTime('now'));
            $details->setOwner($user);
            $manager->persist($details);
        }

        $manager->flush();
    }
    public function getOrder()
    {
        // TODO: Implement getOrder() method.
        return 20;
    }
}