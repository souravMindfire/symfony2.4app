<?php

namespace Mindfire\DataflowBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mindfire\DataflowBundle\Entity\Details;


class DefaultController extends Controller
{

    public function indexAction()
    {
      /*  $details=new Details();
        $details->setName('sohel');
        $details->setTime(new \DateTime('now'));
        $em= $this->getDoctrine()->getEntityManager();
        $em->persist($details);
        $em->flush();*/
        $em= $this->getDoctrine()->getEntityManager();
        $repo=$em->getRepository('DataflowBundle:Details');
       // $users=$repo->findOneBy(array(
       //     'name'=>'sourav'
       // ));

        $users=$repo->findAll();

        return $this->render('DataflowBundle:Default:index.html.twig',array('users'=>$users));
    }
}
