<?php

namespace Mindfire\DataflowBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Mindfire\UsersBundle\Entity\Login;

/**
 * Details
 *
 * @ORM\Table(name="user_details")
 * @ORM\Entity(repositoryClass="Mindfire\DataflowBundle\Entity\DetailsRepository")
 */
class Details
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Mindfire\UsersBundle\Entity\Login")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetimetz")
     */
    private $time;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Details
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     * @return Details
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime 
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return Login
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param Login $owner
     */
    public function setOwner(Login $owner)
    {
        $this->owner = $owner;
    }
}
