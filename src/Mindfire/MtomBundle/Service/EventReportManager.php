<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 12/6/18
 * Time: 11:15 AM
 */

namespace Mindfire\MtomBundle\Service;


class EventReportManager
{
    private $em;

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function getCSV(){

        $events = $this->em->getRepository('MtomBundle:testevents')->findAll();
        $rows=array();
        $csvHead=array('id','event name','time');
        $rows[] = implode(',',$csvHead);
        foreach ($events as $e){
            $data = array(
                $e->getId(),
                $e->getName(),
                $e->getDate()->format('Y-m-d H:i:s')
            );
            $rows[] = implode(',',$data);
        }
        return implode("\n",$rows);
    }
}