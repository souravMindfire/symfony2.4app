<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 11/6/18
 * Time: 6:25 PM
 */

namespace Mindfire\MtomBundle\Controller;
use Mindfire\MtomBundle\Service\EventReportManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ExcelController extends Controller
{
    /**
     * @Route("events/updated.csv")
     */
    public function updatedEventsAction(){
       $em = $this->getDoctrine()->getManager();

        /*  $events = $em->getRepository('MtomBundle:testevents')->findAll();
         $rows=array();
         $csvHead=array('id','event name','time');
         $rows[] = implode(',',$csvHead);
         foreach ($events as $e){
             $data = array(
                 $e->getId(),
                 $e->getName(),
                 $e->getDate()->format('Y-m-d H:i:s')
             );
             $rows[] = implode(',',$data);
         }
         $content = implode("\n",$rows); */
        $eventReportManager = new EventReportManager($em);
        $content = $eventReportManager->getCSV();
        $response = new Response($content);
        $response->headers->set('Content-Type','text/csv');
        return $response;
    }
}