<?php

namespace Mindfire\MtomBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MtomBundle:Default:index.html.twig', array('name' => $name));
    }
}
