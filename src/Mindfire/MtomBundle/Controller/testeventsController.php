<?php

namespace Mindfire\MtomBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Mindfire\MtomBundle\Entity\testevents;
use Mindfire\MtomBundle\Form\testeventsType;


/**
 * testevents controller.
 *
 */
class testeventsController extends Controller
{

    /**
     * Lists all testevents entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MtomBundle:testevents')->findAll();

        return $this->render('MtomBundle:testevents:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new testevents entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new testevents();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('testevents_show', array('id' => $entity->getId())));
        }

        return $this->render('MtomBundle:testevents:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a testevents entity.
     *
     * @param testevents $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(testevents $entity)
    {
        $form = $this->createForm(new testeventsType(), $entity, array(
            'action' => $this->generateUrl('testevents_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new testevents entity.
     *
     */
    public function newAction()
    {
        $entity = new testevents();
        $form   = $this->createCreateForm($entity);

        return $this->render('MtomBundle:testevents:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a testevents entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MtomBundle:testevents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find testevents entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MtomBundle:testevents:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing testevents entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MtomBundle:testevents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find testevents entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MtomBundle:testevents:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a testevents entity.
    *
    * @param testevents $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(testevents $entity)
    {
        $form = $this->createForm(new testeventsType(), $entity, array(
            'action' => $this->generateUrl('testevents_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing testevents entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MtomBundle:testevents')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find testevents entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('testevents_edit', array('id' => $id)));
        }

        return $this->render('MtomBundle:testevents:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a testevents entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MtomBundle:testevents')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find testevents entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('testevents'));
    }

    /**
     * Creates a form to delete a testevents entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('testevents_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }


    public function attendAction($id,$format){

            $em = $this->getDoctrine()->getManager();
            $event = $em->getRepository('MtomBundle:testevents')->find($id);

            if (!$event) {
                throw $this->createNotFoundException('Unable to find testevents entity.');
            }
            if(!$event->hasAttendee($this->getUser())) {
                $event->getAttendees()->add($this->getUser());
                $em->persist($event);
                $em->flush();
            }
            if($format=='json'){
                $data = array(
                    'attending'=>true
                );
                $response= new JsonResponse($data);
                return $response;
            }
            $url=$this->generateUrl('testevents');
            return $this->redirect($url);

    }

    public function unattendAction($id,$format){
        $em = $this->getDoctrine()->getManager();
        $event = $em->getRepository('MtomBundle:testevents')->find($id);

        if (!$event) {
            throw $this->createNotFoundException('Unable to find testevents entity.');
        }
        if($event->hasAttendee($this->getUser())) {
            $event->getAttendees()->removeElement($this->getUser());
            $em->persist($event);
            $em->flush();
        }
        if($format=='json'){
            $data = array(
                'attending'=>false
            );
            $response= new JsonResponse($data);
            return $response;
        }
        $url=$this->generateUrl('testevents');
        return $this->redirect($url);
    }
}
