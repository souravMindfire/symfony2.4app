<?php

namespace Mindfire\UsersBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Login
 *
 * @ORM\Table(name="user_login")
 * @ORM\Entity(repositoryClass="Mindfire\UsersBundle\Entity\LoginRepository")
 * @UniqueEntity(fields="email",message="emailid already in use")
 * @UniqueEntity(fields="username",message="username already in use")
 */
class Login implements UserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="username", type="string", length=255)
     * @Assert\NotBlank(message="Should not be blank")
     * @Assert\Length(min=4, minMessage="Atleast 4 chars")
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\Regex(
     *  pattern="/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/",
     *  message="1 upper,1 lower,1number"
     * )
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email()
     */
    private $email;


    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Login
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Login
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set email
     *
     * @param string $email
     * @return Login
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->password;
    }

    public function getRoles()
    {
       return array('ROLE_ADMIN');
    }
    public function eraseCredentials()
    {

    }
    public function getSalt()
    {
        return null;
    }
}
