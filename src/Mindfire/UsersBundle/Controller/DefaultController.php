<?php

namespace Mindfire\UsersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;


class DefaultController extends Controller
{
    /**
     * @Route("/test",name="test")
     */
    public function indexAction()
    {


    }

    /**
     * @Route("/login_form",name="login_form")
     */
    public function loginAction(Request $request)
    {
        $session=$request->getSession();
        if($request->attributes->has(SecurityContextInterface::AUTHENTICATION_ERROR)){
            $error=$request->attributes->get(
                SecurityContextInterface::AUTHENTICATION_ERROR
            );
        }
        elseif (null !== $session && $session->has(SecurityContextInterface::AUTHENTICATION_ERROR)){
            $error=$session->get(SecurityContextInterface::AUTHENTICATION_ERROR);
            $session->remove(SecurityContextInterface::AUTHENTICATION_ERROR);
        }
        else
            $error='';
        return $this->render('UsersBundle:Security:login.html.twig', array(
            'error'         => $error,
        ));
    }
    /**
     * @Route("/login_check",name="login_check")
     */
    public function loginCheckAction(){
        return $this->render('UsersBundle:Security:login.html.twig');
    }
    /**
     * @Route("/logout",name="logout")
     */
    public function logoutAction(){
        return $this->render('UsersBundle:Security:login.html.twig');
    }
}
