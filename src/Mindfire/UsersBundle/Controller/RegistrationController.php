<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 8/6/18
 * Time: 1:00 PM
 */

namespace Mindfire\UsersBundle\Controller;
use Mindfire\UsersBundle\Entity\Login;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RegistrationController extends Controller implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */

    protected $container;

    /**
     * {@inheritdoc}
     */
    /**
     * @Route("/register",name="register")
     * @Template("UsersBundle:Security:registerUser.html.twig")
     */
    public function registerUserAction(Request $request){
        $form=$this->createFormBuilder()
            ->add('username','text')
            ->add('email','email')
            ->add('password','repeated',array(
                'type'=>'password'
            ))
            ->getForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $data=$form->getData();
            $login=new Login();
            $login->setUsername($data['username']);
            $login->setEmail($data['email']);
            $login->setPassword($this->encodePassword($login,$data['password']));
            $em=$this->getDoctrine()->getManager();
            $em->persist($login);
            $em->flush();
           // $url=$this->generateUrl('users');
           // return $this->redirect($url);
        }
        return array('form'=>$form->createView());
    }


    private function encodePassword(Login $login,$plainPassword){
        $encoder=$this->container->get('security.encoder_factory')->getEncoder($login);
        return $encoder->encodePassword($plainPassword,$login->getSalt());
    }
    public function setContainer(ContainerInterface $container=null){
        $this->container=$container;
    }
}