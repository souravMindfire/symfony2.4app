<?php
/**
 * Created by PhpStorm.
 * User: mindfire
 * Date: 6/6/18
 * Time: 3:15 PM
 */
namespace Mindfire\UsersBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mindfire\UsersBundle\Entity\Login;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadLoginData implements FixtureInterface,ContainerAwareInterface,OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */

    private $container;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {

            $details=new Login();
            $details->setUsername('sourav');
            $details->setPassword($this->encodePassword($details,'password'));
            $details->setEmail('sourav@mindfire.com');
            $manager->persist($details);
            $manager->flush();
    }

    private function encodePassword(Login $login,$plainPassword){
        $encoder=$this->container->get('security.encoder_factory')->getEncoder($login);
        return $encoder->encodePassword($plainPassword,$login->getSalt());
    }

    public function setContainer(ContainerInterface $container=null){
        $this->container=$container;
    }
    public function getOrder()
    {
        // TODO: Implement getOrder() method.
        return 10;
    }
}